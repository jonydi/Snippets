#include <iostream>
#include <string>
 
class foo
{
        public:
 
        foo( void )
        {       
                print( "foo()" );
        }       
 
        ~foo( void )
        {       
                print( "~foo()" );
        }
 
        void print( std::string const& text ) 
        {
                std::cout << static_cast< void* >( this ) << " : " << text << std::endl;
        }
 
/* 
        Deshabilitado el contructor de copia y el operador de asignación al hacerlos privados
*/ 
        private:
 
        foo( foo const& );
 
        foo& operator = ( foo const& );
};
 
int main( void )
{
        foo array[ 3 ];
/* 
        Cuanto la 'main' termina, el destructor es invocado para cada elemento de 'array'.
        La primera instancia creada es la última en ser destruída.
*/
}